package http

import (
	"encoding/json"
	"html/template"
	"log"
	"net/http"
	"os"
	"sync"
	"time"

	"bitbucket.org/Edas_L/benbjohnson/wtf"
	"bitbucket.org/Edas_L/benbjohnson/wtf/bolt"
	"github.com/gorilla/websocket"
	"github.com/julienschmidt/httprouter"
)

var _ Database = &bolt.DialService{}
var clients = make(map[*websocket.Conn]bool)
var broadcast = make(chan Message)
var upgrader = websocket.Upgrader{}
var mutex sync.Mutex

//Message defines our message structure
type Message struct {
	Email    string `json:"email"`
	Username string `json:"username"`
	Message  string `json:"message"`
}

//Database interface
type Database interface {
	Dial(id wtf.DialID) (*wtf.Dial, error)
	CreateDial(d *wtf.Dial) error
	SetLevel(id wtf.DialID, token string, level float64) error
	ListDials() ([]wtf.Dial, error)
	GetAverage() (float64, error)
	DeleteDial(ID wtf.DialID) error
}

// DialHandler represents an HTTP API handler for dials.
type DialHandler struct {
	*httprouter.Router

	DB Database

	Logger *log.Logger

	Template *template.Template

	Index string

	Chat string
}

// NewDialHandler returns a new instance of DialHandler.
func NewDialHandler(layoutTmpl string, indexLoc string, chatLoc string) *DialHandler {
	t, err := template.ParseFiles(layoutTmpl)
	if err != nil {
		log.Fatal(err)
	}

	h := &DialHandler{
		Router:   httprouter.New(),
		Logger:   log.New(os.Stderr, "", log.LstdFlags),
		Template: t,
		Index:    indexLoc,
		Chat:     chatLoc,
	}

	h.POST("/api/dials", h.handlePostDial)
	h.GET("/api/dials/:id", h.handleGetDial)
	h.GET("/list2", h.handleGetAll2Dial)
	h.GET("/list", h.handleGetAllDial)
	h.GET("/", h.handleGetHTML)
	h.GET("/chat", h.handleGetChat)
	h.GET("/ws", h.handleConnections)
	h.DELETE("/api/dials/:id", h.handleDeleteDial)
	h.PATCH("/api/dials/:id", h.handlePatchDial)
	return h
}

func (h *DialHandler) handleGetChat(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	http.ServeFile(w, r, h.Chat)
	go handleMessages()

}
func handleMessages() {
	for {
		// Grab the next message from the broadcast channel
		msg := <-broadcast

		// Send it out to every client that is currently connected
		mutex.Lock()
		for client := range clients {
			err := client.WriteJSON(msg)

			if err != nil {
				log.Printf("error: %v", err)
				client.Close()
				delete(clients, client)
			}
		}
		mutex.Unlock()
	}
}

func (h *DialHandler) handleConnections(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

	// Upgrade initial GET request to a websocket
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Fatal(err)
	}

	// Make sure we close the connection when the function returns
	defer ws.Close()

	mutex.Lock()
	clients[ws] = true
	mutex.Unlock()

	for {
		var msg Message
		// Read in a new message as JSON and map it to a Message object
		err := ws.ReadJSON(&msg)
		if err != nil {
			log.Printf("error: %v", err)
			mutex.Lock()
			delete(clients, ws)
			mutex.Unlock()
			break
		}
		// Send the newly received message to the broadcast channel
		broadcast <- msg
	}
}

func (h *DialHandler) handleGetHTML(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	http.ServeFile(w, r, h.Index)
}

func (h *DialHandler) handleDeleteDial(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

	id := ps.ByName("id")
	err := h.DB.DeleteDial(wtf.DialID(id))
	switch err {
	case nil:
		encodeJSON(w, &deleteDialResponse{Msg: "Dial deleted"}, h.Logger)
	case wtf.ErrDialRequired, wtf.ErrDialIDRequired:
		Error(w, err, http.StatusBadRequest, h.Logger)
	case wtf.ErrDialExists:
		Error(w, err, http.StatusConflict, h.Logger)
	default:
		Error(w, err, http.StatusInternalServerError, h.Logger)
	}

}

// handleGetDial handles requests to create a new dial.
func (h *DialHandler) handlePostDial(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	if origin := r.Header.Get("Origin"); origin != "" {
		w.Header().Set("Access-Control-Allow-Origin", origin)
	}
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token")
	w.Header().Set("Access-Control-Allow-Credentials", "true")

	var req postDialRequest
	if r.Body == nil {
		Error(w, wtf.ErrDialNotFound, http.StatusBadRequest, h.Logger)
		return
	}
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		Error(w, wtf.ErrInvalidJSON, http.StatusBadRequest, h.Logger)
		return
	}
	d := req.Dial
	d.Token = req.Token
	d.ModTime = time.Time{}

	// Create dial.
	switch err := h.DB.CreateDial(d); err {
	case nil:
		encodeJSON(w, &postDialResponse{Dial: d}, h.Logger)
	case wtf.ErrDialRequired, wtf.ErrDialIDRequired, wtf.ErrDialLevelRequired:
		Error(w, err, http.StatusBadRequest, h.Logger)
	case wtf.ErrDialExists:
		Error(w, err, http.StatusConflict, h.Logger)
	default:
		Error(w, err, http.StatusInternalServerError, h.Logger)
	}
}

type postDialRequest struct {
	Dial  *wtf.Dial `json:"dial,omitempty"`
	Token string    `json:"token,omitempty"`
}

type postDialResponse struct {
	Dial *wtf.Dial `json:"dial,omitempty"`
	Err  string    `json:"err,omitempty"`
}
type deleteDialRequest struct {
	DialID *wtf.DialID `json:"dialid,omitempty"`
	Err    string      `json:"err,omitempty"`
}
type deleteDialResponse struct {
	Msg string `json:"string,omitempty"`
	Err string `json:"err,omitempty"`
}

// handleGetDial handles requests to fetch a single dial.
func (h *DialHandler) handleGetDial(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	// Find dial by ID.
	d, err := h.DB.Dial(wtf.DialID(id))
	if err != nil {
		Error(w, err, http.StatusInternalServerError, h.Logger)
	} else if d == nil {
		NotFound(w)
	} else {
		encodeJSON(w, &getDialResponse{Dial: d}, h.Logger)
	}
}
func (h *DialHandler) handleGetAllDial(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	d, err := h.DB.ListDials()
	if err != nil {
		Error(w, err, http.StatusInternalServerError, h.Logger)
	} else if d == nil {
		NotFound(w)
	}
	h.Template.Execute(w, d)

}
func (h *DialHandler) handleGetAll2Dial(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	if origin := r.Header.Get("Origin"); origin != "" {
		w.Header().Set("Access-Control-Allow-Origin", origin)
	}
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token")
	w.Header().Set("Access-Control-Allow-Credentials", "true")

	d, err := h.DB.ListDials()
	if err != nil {
		Error(w, wtf.ErrDialNotFound, http.StatusInternalServerError, h.Logger)
	} else if d == nil {
		NotFound(w)
	}
	encodeJSON(w, &getDialAllResponse{Dials: d}, h.Logger)

}

type getDialResponse struct {
	Dial *wtf.Dial `json:"dial,omitempty"`
	Err  string    `json:"err,omitempty"`
}
type getDialAllResponse struct {
	Dials []wtf.Dial `json:"dials, omitempty`
	Err   string     `json:"err,omitempty"`
}
type getStringResponse struct {
	Avr    string  `json:"string`
	Number float64 `json:"float64,omitempty"`
	Err    string  `json:"err,omitempty"`
}

func (h *DialHandler) handlePatchDial(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	// Decode request.
	var req patchDialRequest
	if r.Body == nil {
		Error(w, wtf.ErrDialNotFound, http.StatusBadRequest, h.Logger)
		return
	}
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		Error(w, wtf.ErrInvalidJSON, http.StatusBadRequest, h.Logger)
		return
	}
	// Create dial.
	switch err := h.DB.SetLevel(req.ID, req.Token, req.Level); err {
	case nil:
		encodeJSON(w, &patchDialResponse{}, h.Logger)
	case wtf.ErrDialNotFound:
		Error(w, err, http.StatusNotFound, h.Logger)
	case wtf.ErrUnauthorized:
		Error(w, err, http.StatusUnauthorized, h.Logger)
	default:
		Error(w, err, http.StatusInternalServerError, h.Logger)
	}
}

type patchDialRequest struct {
	ID    wtf.DialID `json:"id"`
	Token string     `json:"token"`
	Level float64    `json:"level"`
}

type patchDialResponse struct {
	Err string `json:"err,omitempty"`
}

//Error returns error response to the server
func Error(w http.ResponseWriter, err error, code int, logger *log.Logger) {
	// Log error.
	logger.Printf("http error: %s (code=%d)", err, code)

	// Hide error from client if it is internal.
	if code == http.StatusInternalServerError {
		err = wtf.ErrInternal
	}

	// Write generic error response.
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(&errorResponse{Err: err.Error()})
}

// errorResponse is a generic response for sending a error.
type errorResponse struct {
	Err string `json:"err,omitempty"`
}

//NotFound error
func NotFound(w http.ResponseWriter) {
	w.WriteHeader(http.StatusNotFound)
	w.Write([]byte(`{}` + "\n"))
}
func encodeJSON(w http.ResponseWriter, v interface{}, logger *log.Logger) {
	if err := json.NewEncoder(w).Encode(v); err != nil {
		Error(w, err, http.StatusInternalServerError, logger)
	}
}
