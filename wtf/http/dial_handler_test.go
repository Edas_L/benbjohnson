package http

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"bitbucket.org/Edas_L/benbjohnson/wtf/http/mocks"

	"bitbucket.org/Edas_L/benbjohnson/wtf"
	"github.com/golang/mock/gomock"
)

func TestNewDialHandler_NotFound(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockDB := mocks.NewMockDatabase(mockCtrl)
	mockDB.EXPECT().Dial(wtf.DialID("1")).Return(nil, nil)

	handler := NewDialHandler("/home/edas/go/src/bitbucket.org/Edas_L/benbjohnson/wtf/http/layout.html", "/home/edas/go/src/bitbucket.org/Edas_L/benbjohnson/index.html", "/home/edas/go/src/bitbucket.org/Edas_L/benbjohnson/wtf/http/chat/chat.html")
	handler.DB = mockDB
	rr := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/api/dials/1", nil)
	if err != nil {
		t.Fatal(err)
	}

	handler.ServeHTTP(rr, req)

	expected := `{}`
	if status := rr.Code; status != 404 {
		t.Fatalf("handler returned wrong status code: got %v want %v", status, 404)
	}
	if strings.Compare(strings.TrimRight(rr.Body.String(), "\n"), expected) != 0 {
		t.Fatalf("responsed dial was incorrect, got %v, wanted %v", rr.Body.String(), expected)
	}

}
func TestNewDialHandler_DifferentDial(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockDB := mocks.NewMockDatabase(mockCtrl)
	mockDB.EXPECT().Dial(wtf.DialID("2")).Return(&wtf.Dial{ID: "2"}, nil)

	handler := NewDialHandler("/home/edas/go/src/bitbucket.org/Edas_L/benbjohnson/wtf/http/layout.html", "/home/edas/go/src/bitbucket.org/Edas_L/benbjohnson/index.html", "/home/edas/go/src/bitbucket.org/Edas_L/benbjohnson/wtf/http/chat/chat.html")
	handler.DB = mockDB
	rr := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/api/dials/2", nil)
	if err != nil {
		t.Fatal(err)
	}
	handler.ServeHTTP(rr, req)
	expected := `{"dial":{"dialID":"2","level":0,"modTime":"0001-01-01T00:00:00Z"}}`
	if status := rr.Code; status != http.StatusOK {
		t.Fatalf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}
	if strings.Compare(strings.TrimRight(rr.Body.String(), "\n"), expected) != 0 {
		t.Fatalf("responsed dial was incorrect, got %v, wanted %v", rr.Body.String(), expected)
	}

}

func TestDialHandler_CreateDial_NilRequest(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockDB := mocks.NewMockDatabase(mockCtrl)

	handler := NewDialHandler("/home/edas/go/src/bitbucket.org/Edas_L/benbjohnson/wtf/http/layout.html", "/home/edas/go/src/bitbucket.org/Edas_L/benbjohnson/index.html", "/home/edas/go/src/bitbucket.org/Edas_L/benbjohnson/wtf/http/chat/chat.html")
	handler.DB = mockDB
	rr := httptest.NewRecorder()
	req, err := http.NewRequest("POST", "/api/dials", nil)
	if err != nil {
		t.Fatal(err)
	}
	handler.ServeHTTP(rr, req)
	if rr.Code != http.StatusBadRequest {
		t.Fatalf("handler returned wrong status code: got %v want %v", rr.Code, http.StatusBadRequest)
	}

}
func TestDialHandler_CreateDial(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockDB := mocks.NewMockDatabase(mockCtrl)
	mockDB.EXPECT().CreateDial(&wtf.Dial{ID: "2"}).Return(nil)

	handler := NewDialHandler("/home/edas/go/src/bitbucket.org/Edas_L/benbjohnson/wtf/http/layout.html", "/home/edas/go/src/bitbucket.org/Edas_L/benbjohnson/index.html", "/home/edas/go/src/bitbucket.org/Edas_L/benbjohnson/wtf/http/chat/chat.html")
	handler.DB = mockDB
	rr := httptest.NewRecorder()
	dial := `{"dial":{"dialID":"2","level":0,"modTime":"0001-01-01T00:00:00Z"}}`
	req, err := http.NewRequest("POST", "/api/dials", strings.NewReader(dial))
	if err != nil {
		t.Fatal(err)
	}
	handler.ServeHTTP(rr, req)
	if rr.Code != http.StatusOK {
		t.Fatalf("handler returned wrong status code: got %v want %v", rr.Code, http.StatusOK)
	}
	if strings.Compare(strings.TrimRight(rr.Body.String(), "\n"), dial) != 0 {
		t.Fatalf("responsed dial was incorrect, got %v, wanted %v", rr.Body.String(), dial)
	}

}

func TestDialHandler_ListDials(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockDB := mocks.NewMockDatabase(mockCtrl)
	mockDB.EXPECT().ListDials().Return([]wtf.Dial{
		wtf.Dial{ID: "YYYY"},
		wtf.Dial{ID: "ZZZZ"},
	}, nil)
	mockDB.EXPECT().GetAverage().Return(5.0, nil)

	handler := NewDialHandler("/home/edas/go/src/bitbucket.org/Edas_L/benbjohnson/wtf/http/layout.html", "/home/edas/go/src/bitbucket.org/Edas_L/benbjohnson/index.html", "/home/edas/go/src/bitbucket.org/Edas_L/benbjohnson/wtf/http/chat/chat.html")
	handler.DB = mockDB

	rr := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/list", nil)
	if err != nil {
		t.Fatal(err)
	}
	handler.ServeHTTP(rr, req)
	if rr.Code != http.StatusOK {
		t.Fatalf("handler returned wrogn status code: wanted %v, got %v", http.StatusOK, rr.Code)
	}
	expected := `{"Dials":[{"dialID":"YYYY","level":0,"modTime":"0001-01-01T00:00:00Z"},{"dialID":"ZZZZ","level":0,"modTime":"0001-01-01T00:00:00Z"}]}
{"Avr":"Vidurkis:","float64":5}`
	if strings.Compare(strings.TrimRight(rr.Body.String(), "\n"), expected) != 0 {
		t.Fatalf("responsed dials were incorrect, got \n %v \n, wanted \n %v", rr.Body.String(), expected)
	}
}

func TestDialHandler_SetLevel(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockDB := mocks.NewMockDatabase(mockCtrl)
	mockDB.EXPECT().SetLevel(wtf.DialID("3"), "T2", 500.0).Return(nil)

	handler := NewDialHandler("/home/edas/go/src/bitbucket.org/Edas_L/benbjohnson/wtf/http/layout.html", "/home/edas/go/src/bitbucket.org/Edas_L/benbjohnson/index.html", "/home/edas/go/src/bitbucket.org/Edas_L/benbjohnson/wtf/http/chat/chat.html")
	handler.DB = mockDB
	rr := httptest.NewRecorder()
	dialLevel := `{"id": "3", "token" : "T2", "level" : 500}`
	req, err := http.NewRequest("PATCH", "/api/dials/3", strings.NewReader(dialLevel))
	if err != nil {
		t.Fatal(err)
	}
	handler.ServeHTTP(rr, req)
	if rr.Code != http.StatusOK {
		t.Fatalf("handler returned wrong status code: wanted %v, got %v", http.StatusOK, rr.Code)
	}
}
func TestDialHandler_SetLevel_NilRequest(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockDB := mocks.NewMockDatabase(mockCtrl)

	handler := NewDialHandler("/home/edas/go/src/bitbucket.org/Edas_L/benbjohnson/wtf/http/layout.html", "/home/edas/go/src/bitbucket.org/Edas_L/benbjohnson/index.html", "/home/edas/go/src/bitbucket.org/Edas_L/benbjohnson/wtf/http/chat/chat.html")
	handler.DB = mockDB

	rr := httptest.NewRecorder()
	req, err := http.NewRequest("PATCH", "/api/dials/2", nil)
	if err != nil {
		t.Fatal(err)
	}
	handler.ServeHTTP(rr, req)
	if rr.Code != http.StatusBadRequest {
		t.Fatalf("handler returned wrong status code: wanted %v, got %v", http.StatusBadRequest, rr.Code)
	}
}

func TestDialhandler_DeleteDial(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockDB := mocks.NewMockDatabase(mockCtrl)
	mockDB.EXPECT().DeleteDial(wtf.DialID("1")).Return(nil)

	handler := NewDialHandler("/home/edas/go/src/bitbucket.org/Edas_L/benbjohnson/wtf/http/layout.html", "/home/edas/go/src/bitbucket.org/Edas_L/benbjohnson/index.html", "/home/edas/go/src/bitbucket.org/Edas_L/benbjohnson/wtf/http/chat/chat.html")
	handler.DB = mockDB

	rr := httptest.NewRecorder()
	req, err := http.NewRequest("DELETE", "/api/dials/1", nil)
	if err != nil {
		t.Fatal(err)
	}
	handler.ServeHTTP(rr, req)
	if rr.Code != http.StatusOK {
		t.Fatalf("handler returned wrogn status code: wanted %v, got %v", http.StatusOK, rr.Code)
	}
	expected := `{"string":"Dial deleted"}`
	if strings.Compare(strings.TrimRight(rr.Body.String(), "\n"), expected) != 0 {
		t.Fatalf("Expected to get %v, got %v", expected, rr.Body)
	}
}
