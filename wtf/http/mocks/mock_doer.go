// Code generated by MockGen. DO NOT EDIT.
// Source: github.com/benbjohnson/wtf/http (interfaces: Database)

// Package mocks is a generated GoMock package.
package mocks

import (
	reflect "reflect"

	wtf "bitbucket.org/Edas_L/benbjohnson/wtf"
	gomock "github.com/golang/mock/gomock"
)

// MockDatabase is a mock of Database interface
type MockDatabase struct {
	ctrl     *gomock.Controller
	recorder *MockDatabaseMockRecorder
}

// MockDatabaseMockRecorder is the mock recorder for MockDatabase
type MockDatabaseMockRecorder struct {
	mock *MockDatabase
}

// NewMockDatabase creates a new mock instance
func NewMockDatabase(ctrl *gomock.Controller) *MockDatabase {
	mock := &MockDatabase{ctrl: ctrl}
	mock.recorder = &MockDatabaseMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockDatabase) EXPECT() *MockDatabaseMockRecorder {
	return m.recorder
}

// CreateDial mocks base method
func (m *MockDatabase) CreateDial(arg0 *wtf.Dial) error {
	ret := m.ctrl.Call(m, "CreateDial", arg0)
	ret0, _ := ret[0].(error)
	return ret0
}

// CreateDial indicates an expected call of CreateDial
func (mr *MockDatabaseMockRecorder) CreateDial(arg0 interface{}) *gomock.Call {
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CreateDial", reflect.TypeOf((*MockDatabase)(nil).CreateDial), arg0)
}

// DeleteDial mocks base method
func (m *MockDatabase) DeleteDial(arg0 wtf.DialID) error {
	ret := m.ctrl.Call(m, "DeleteDial", arg0)
	ret0, _ := ret[0].(error)
	return ret0
}

// DeleteDial indicates an expected call of DeleteDial
func (mr *MockDatabaseMockRecorder) DeleteDial(arg0 interface{}) *gomock.Call {
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "DeleteDial", reflect.TypeOf((*MockDatabase)(nil).DeleteDial), arg0)
}

// Dial mocks base method
func (m *MockDatabase) Dial(arg0 wtf.DialID) (*wtf.Dial, error) {
	ret := m.ctrl.Call(m, "Dial", arg0)
	ret0, _ := ret[0].(*wtf.Dial)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Dial indicates an expected call of Dial
func (mr *MockDatabaseMockRecorder) Dial(arg0 interface{}) *gomock.Call {
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Dial", reflect.TypeOf((*MockDatabase)(nil).Dial), arg0)
}

// GetAverage mocks base method
func (m *MockDatabase) GetAverage() (float64, error) {
	ret := m.ctrl.Call(m, "GetAverage")
	ret0, _ := ret[0].(float64)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetAverage indicates an expected call of GetAverage
func (mr *MockDatabaseMockRecorder) GetAverage() *gomock.Call {
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetAverage", reflect.TypeOf((*MockDatabase)(nil).GetAverage))
}

// ListDials mocks base method
func (m *MockDatabase) ListDials() ([]wtf.Dial, error) {
	ret := m.ctrl.Call(m, "ListDials")
	ret0, _ := ret[0].([]wtf.Dial)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// ListDials indicates an expected call of ListDials
func (mr *MockDatabaseMockRecorder) ListDials() *gomock.Call {
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ListDials", reflect.TypeOf((*MockDatabase)(nil).ListDials))
}

// SetLevel mocks base method
func (m *MockDatabase) SetLevel(arg0 wtf.DialID, arg1 string, arg2 float64) error {
	ret := m.ctrl.Call(m, "SetLevel", arg0, arg1, arg2)
	ret0, _ := ret[0].(error)
	return ret0
}

// SetLevel indicates an expected call of SetLevel
func (mr *MockDatabaseMockRecorder) SetLevel(arg0, arg1, arg2 interface{}) *gomock.Call {
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SetLevel", reflect.TypeOf((*MockDatabase)(nil).SetLevel), arg0, arg1, arg2)
}
