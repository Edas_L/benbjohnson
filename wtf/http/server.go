package http

import (
	"net"
	"net/http"
)

// Server represents an HTTP server.
type Server struct {
	Ln net.Listener

	// Handler to serve.
	Handler *Handler

	// Bind address to open.
	Addr string
}

// Open opens a socket and serves the HTTP server.
func (s *Server) Open() error {
	// Open socket.
	ln, err := net.Listen("tcp", s.Addr)
	if err != nil {
		return err
	}
	s.Ln = ln

	// Start HTTP server.
	go func() { http.Serve(s.Ln, s.Handler) }()

	return nil
}

// Close closes the socket.
func (s *Server) Close() error {
	if s.Ln != nil {
		s.Ln.Close()
	}
	return nil
}
