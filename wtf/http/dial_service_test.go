package http

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"bitbucket.org/Edas_L/benbjohnson/wtf"
	"github.com/stretchr/testify/assert"
)

func TestDialService_CreateDial(t *testing.T) {
	testServer := httptest.NewServer(&mockServer{})
	defer testServer.Close()

	url, err := url.ParseRequestURI(testServer.URL)
	if err != nil {
		t.Fatal(err)
	}

	dialSvc := DialService{
		URL: url,
	}
	dial := wtf.Dial{ID: "XXX"}
	err = dialSvc.CreateDial(&dial)
	if err != nil {
		t.Fatal(err)
	}
	if dial.Level != 1 {
		t.Errorf("Level should be 1, got %v", dial.Level)
	}
	dial2, err := dialSvc.Dial(dial.ID)
	if err != nil {
		t.Fatal(err)
	}
	if dial2.Level != 1 {
		t.Errorf("Level should be 1, got %v", dial.Level)
	}
}
func TestDialService_CreateDial_NilDial(t *testing.T) {
	testServer := httptest.NewServer(&mockServer{})
	defer testServer.Close()

	url, err := url.ParseRequestURI(testServer.URL)
	if err != nil {
		t.Fatal(err)
	}

	dialSvc := DialService{
		URL: url,
	}
	err = dialSvc.CreateDial(nil)
	if err != wtf.ErrDialRequired {
		t.Fatalf("Error should be %v, got %v", wtf.ErrDialRequired, err)
	}
}

func TestDialService_DeleteDial(t *testing.T) {
	testServer := httptest.NewServer(&mockServer2{})
	defer testServer.Close()

	url, err := url.ParseRequestURI(testServer.URL)
	if err != nil {
		t.Fatal(err)
	}

	dialSvc := DialService{
		URL: url,
	}
	err = dialSvc.DeleteDial("XXX")

	if err != nil {
		t.Fatal(err)
	}

}
func TestDialService_DeleteDial_NilDialID(t *testing.T) {
	testServer := httptest.NewServer(&mockServer2{})
	defer testServer.Close()

	url, err := url.ParseRequestURI(testServer.URL)
	if err != nil {
		t.Fatal(err)
	}

	dialSvc := DialService{
		URL: url,
	}
	err = dialSvc.DeleteDial("")

	if err != wtf.ErrDialIDRequired {
		t.Fatal(err)
	}

}
func TestDialService_SetLevel(t *testing.T) {
	testServer := httptest.NewServer(&mockServer2{})
	defer testServer.Close()

	url, err := url.ParseRequestURI(testServer.URL)
	assert.Nil(t, err)

	dialSvc := DialService{
		URL: url,
	}
	dial := wtf.Dial{
		ID:    "XO",
		Token: "T1",
		Level: 500,
	}
	err = dialSvc.SetLevel(dial.ID, dial.Token, dial.Level)
	if err != nil {
		t.Fatal(err)
	}
}

type mockServer struct {
}

func (ms *mockServer) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	dial := `{"dial":{"dialID":"XXX","level":1,"modTime":"0001-01-01T00:00:00Z"}}`
	rw.Write([]byte(dial))

	return
}

type mockServer2 struct {
}

func (ms *mockServer2) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	msg := `{"string":"Dial deleted"}`
	rw.Write([]byte(msg))

	return
}

type mockServer3 struct {
}

func (ms *mockServer3) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	return
}
