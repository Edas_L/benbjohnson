package wtf

// General errors.
const (
	ErrUnauthorized = Error("unauthorized")
)

// Dial errors.
const (
	ErrDialNotFound      = Error("dial not found")
	ErrDialExists        = Error("dial already exists")
	ErrDialIDRequired    = Error("dial id required")
	ErrDialRequired      = Error("dial required")
	ErrInvalidJSON       = Error("Invalid JSON")
	ErrInternal          = Error("Internal error")
	ErrParsingFile       = Error("Could not parse file")
	ErrLevelParsing      = Error("Failed to parse level")
	ErrDialLevelRequired = Error("Dial level required")
)

// Error represents a WTF error.
type Error string

// Error returns the error message.
func (e Error) Error() string { return string(e) }
