package main

import (
	"flag"
	"fmt"
	"log"
	"time"

	"bitbucket.org/Edas_L/benbjohnson/wtf/bolt"
	"bitbucket.org/Edas_L/benbjohnson/wtf/http"
)

func main() {

	wordAddr := flag.String("addr", "foo", "a string")
	wordLayout := flag.String("layout", "foo", "a string")
	wordIndex := flag.String("index", "foo", "a string")
	wordChat := flag.String("chat", "foo", "a string")
	flag.Parse()

	Client := bolt.NewClient()
	Client.Path = ".\\db.db"
	err := Client.Open()
	if err != nil {
		log.Fatal(err)
	}
	defer Client.Close()

	dialSvc := Client.DialService()

	handler := &http.Handler{
		DialHandler: http.NewDialHandler(*wordLayout, *wordIndex, *wordChat),
	}
	handler.DialHandler.DB = dialSvc

	server := http.Server{Addr: *wordAddr, Handler: handler}
	err = server.Open()
	if err != nil {
		fmt.Println(err)
	}
	defer server.Close()

	//Run forever
	go func() {
		time.Sleep(time.Second)
	}()
	select {}

}
